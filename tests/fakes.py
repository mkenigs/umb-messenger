"""Fake classes."""
# pylint: disable=too-few-public-methods
import unittest.mock as mock

import gitlab


class FakePipelineJob():
    """Fake GitLab pipeline job."""

    def __init__(self, job_id, index, attributes):
        """Initialize."""
        self.attributes = {'id': job_id,
                           'web_url': f'joburl/{index}'}
        for key, value in attributes.items():
            self.attributes[key] = value


class FakePipelineJobs:
    """Fake GitLab pipeline jobs."""

    def __init__(self):
        """Initialize."""
        self._jobs = []

    def add_job(self, job_id, attributes):
        """Add a job."""
        self._jobs.append(FakePipelineJob(job_id, len(self._jobs), attributes))

    def list(self, **_):
        """Get all jobs."""
        return self._jobs


class FakeVariable:
    """Fake GitLab variable."""

    def __init__(self, key, value):
        """Initialize."""
        self.key = key
        self.value = value


class FakePipelineVariables:
    """Fake GitLab pipeline variables."""

    def __init__(self, variables):
        """Initialize."""
        self._vars = []
        if variables:  # Handle None
            for key, value in variables.items():
                self._vars.append(FakeVariable(key, value))

    def list(self, **_):
        """Get all variables."""
        return self._vars


class FakePipeline:
    """Fake GitLab pipeline."""

    def __init__(self, status, index, variables=None):
        """Initialize."""
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status,
                           'web_url': f'pipelineurl/{index}',
                           'id': index}
        self.variables = FakePipelineVariables(variables)


class FakeProjectJob():
    """Fake GitLab project job."""

    def __init__(self, job_id, index, attributes):
        """Initialize."""
        self.attributes = {'id': job_id,
                           'web_url': f'joburl/{index}'}
        for key, value in attributes.items():
            self.attributes[key] = value

        self._artifact_data = {}

    def artifact(self, name):
        """Return an artifact."""
        try:
            return self._artifact_data[name]
        except KeyError:
            raise gitlab.GitlabGetError('artifact not found')


class FakeProjectJobs:
    """Fake GitLab project jobs."""

    def __init__(self):
        """Initialize."""
        self._jobs = []

    def add_job(self, job_id, attributes):
        """Add a job."""
        self._jobs.append(FakeProjectJob(job_id, len(self._jobs), attributes))

    def list(self, **_):
        """Get all jobs."""
        return self._jobs

    def get(self, job_id):
        """Get a job."""
        for job in self._jobs:
            if job.attributes['id'] == job_id:
                return job

        return None


class FakeProjectPipelines:
    """Fake GitLab project pipelines."""

    def __init__(self):
        """Initialize."""
        self._pipelines = []

    def add_new_pipeline(self, status, variables=None):
        """Add a fake pipeline."""
        self._pipelines.append(
            FakePipeline(status, len(self._pipelines), variables)
        )

    def get(self, index):
        """Get a pipeline."""
        return self._pipelines[index]

    def list(self, **_):
        """Get all pipelines."""
        return self._pipelines


class FakeMergeRequest:
    """Fake merge request."""

    def __init__(self, attributes):
        """Initialize."""
        for key, value in attributes.items():
            setattr(self, key, value)


class FakeProjectMergeRequests:
    """Fake GitLab merge requests."""

    def __init__(self):
        """Initialize."""
        self._mrs = []

    def add_new_mr(self, attributes):
        """Add a new fake MR."""
        self._mrs.append(FakeMergeRequest(attributes))

    def get(self, mr_id):
        """Get a fake merge request with given ID."""
        for fake_mr in self._mrs:
            if fake_mr.iid == mr_id:
                return fake_mr
        return None


class FakeGitLabProject:
    """Fake GitLab project."""

    def __init__(self):
        """Initialize."""
        self.manager = mock.MagicMock()
        self.pipelines = FakeProjectPipelines()
        self.jobs = FakeProjectJobs()
        self.mergerequests = FakeProjectMergeRequests()


class FakeGitLab:
    """Fake GitLab."""

    def __init__(self):
        """Initialize."""
        self.projects = {}

    def add_project(self, project_name):
        """Add a project."""
        self.projects[project_name] = FakeGitLabProject()
