"""Tests for umb_messenger.main."""
# pylint: disable=no-self-use
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import main


class TestHandleJobEvent(unittest.TestCase):
    """Tests for main.handle_job_event()."""
    @mock.patch('umb_messenger.helpers.all_jobs_succeeded')
    @mock.patch('umb_messenger.umb.handle_message')
    def test_message(self, mock_send, mock_success):
        """
        Verify all functions are called with expected arguments if we're
        supposed to send a message.
        """

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_pre': 'True'}
        )
        pipe = fake_gitlab.projects.get('project').pipelines.get(0)

        main.handle_job_event(0, 'project', {}, fake_gitlab)

        mock_success.assert_called_once_with(
            fake_gitlab.projects.get('project'), pipe, 'setup', None
        )
        mock_send.assert_called_once_with(
            'ready_for_test', mock.ANY, pipe, {}, 'pre'
        )

    @mock.patch('umb_messenger.helpers.all_jobs_succeeded')
    @mock.patch('umb_messenger.umb.handle_message', mock.Mock())
    def test_retriggered_pipeline(self, mock_send):
        """Verify no messages are sent for retriggered pipelines."""

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_pre': 'True', 'retrigger': 'True'}
        )

        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.handle_job_event(0, 'project', {}, fake_gitlab)
            self.assertIn('Skipping pipeline', log.output[-1])
        mock_send.assert_not_called()

    def test_no_pre_test(self):
        """Verify pre-test mesages are not sent if not configured."""
        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'fail', {}
        )

        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.handle_job_event(0, 'project', {}, fake_gitlab)
            self.assertIn('Not configured for pre-test', log.output[-1])

    @mock.patch('umb_messenger.helpers.all_jobs_succeeded')
    def test_failed(self, mock_success):
        """Verify pre-test mesages are not sent if setup jobs failed."""
        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'fail', {'send_ready_for_test_pre': 'True'}
        )
        mock_success.return_value = []

        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.handle_job_event(0, 'project', {}, fake_gitlab)
            self.assertIn('Not ready for pre-test notifications',
                          log.output[-1])


class TestHandleFinished(unittest.TestCase):
    """Tests for main.handle_finished_pipeline()."""
    @mock.patch('umb_messenger.umb.handle_message')
    def test_osci_message(self, mock_send):
        """
        Verify all functions are called if we're supposed to send an OSCI
        message.
        """
        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'is_scratch': 'False'}
        )

        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.handle_finished_pipeline(0, 'project', {}, fake_gitlab)
            self.assertIn('Not configured for post-test', log.output[-1])

        mock_send.assert_called_once()

    @mock.patch('umb_messenger.umb.handle_message')
    def test_post_test_message(self, mock_send):
        """
        Verify all functions are called if we're supposed to send a post-test
        message.
        """
        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_post': 'True'}
        )

        main.handle_finished_pipeline(0, 'project', {}, fake_gitlab)
        mock_send.assert_called_once()

    @mock.patch('umb_messenger.umb.handle_message')
    def test_retriggered_pipeline(self, mock_send):
        """Verify no messages are sent for retriggered pipelines."""
        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_post': 'True', 'retrigger': 'True'}
        )

        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.handle_finished_pipeline(0, 'project', {}, fake_gitlab)
            self.assertIn('Skipping pipeline', log.output[-1])
        mock_send.assert_not_called()

    @mock.patch('umb_messenger.umb.handle_message')
    def test_no_message(self, mock_send):
        """Verify we don't send any messages when we're not supposed to."""

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline('success')

        main.handle_finished_pipeline(0, 'project', {}, fake_gitlab)
        mock_send.assert_not_called()


@mock.patch('umb_messenger.umb.load_configs', mock.Mock())
@mock.patch('umb_messenger.main.get_instance', mock.Mock())
class TestProcessCli(unittest.TestCase):
    """Tests for process_cli."""

    def test_bad_data(self):
        """
        Verify we don't do anything if we are missing parameters or get an
        unknown hook type.
        """
        params = ['--hook-type', 'bad-one',
                  '--id', '11']
        self.assertRaises(SystemExit, lambda: main.main(params))

        params = ['--gitlab-url', 'https://instance/project',
                  '--hook-type', 'bad-one',
                  '--id', '11',
                  '--project', 'thisone']
        with self.assertLogs(main.LOGGER, 'INFO') as log:
            main.main(params)
            self.assertIn('Unknown hook type', log.output[-1])

    @mock.patch('umb_messenger.main.handle_job_event')
    def test_job_call(self, mock_job):
        """Verify handle_job_event is called with the correct parameters."""
        params = ['--gitlab-url', 'https://instance/project',
                  '--hook-type', 'job',
                  '--id', '11',
                  '--project', 'thisone']
        main.main(params)
        mock_job.assert_called_once_with(11, 'thisone', mock.ANY, mock.ANY)

    @mock.patch('umb_messenger.main.handle_finished_pipeline')
    def test_pipeline_call(self, mock_pipe):
        """
        Verify handle_finished_pipeline is called with the correct parameters.
        """
        params = ['--gitlab-url', 'https://instance/project',
                  '--hook-type', 'pipeline',
                  '--id', '11',
                  '--project', 'thisone']
        main.main(params)
        mock_pipe.assert_called_once_with(11, 'thisone', mock.ANY, mock.ANY)


@mock.patch('umb_messenger.umb.load_configs', mock.Mock())
class TestProcessMessage(unittest.TestCase):
    """Tests for main.process_message."""

    def test_bad_data(self):
        """Verify we don't do anything if we get an unknown hook type."""

        params = {'object_kind': 'unknown'}
        with self.assertLogs(main.LOGGER, 'WARNING') as log:
            main.process_message(mock.Mock(), params)
            self.assertIn('Unknown hook type', log.output[-1])

    @mock.patch('umb_messenger.main.handle_job_event')
    def test_job_call(self, mock_job):
        """
        Verify handle_job_event is called with the correct parameters and
        skipped when it should be.
        """
        params = {'object_kind': 'build',
                  'build_stage': 'setup',
                  'build_name': 'setup x86_64',
                  'build_status': 'success',
                  'pipeline_id': 11,
                  'project_id': 3,
                  'repository': {'homepage': 'http://instance/project'}}
        main.process_message(mock.Mock(), params)
        mock_job.assert_called_once_with(
            11, 3, mock.ANY, mock.ANY, 'setup x86_64'
        )

        mock_job.reset_mock()
        params['build_stage'] = 'test'
        main.process_message(mock.Mock(), params)
        mock_job.assert_not_called()

    @mock.patch('umb_messenger.main.handle_finished_pipeline')
    def test_pipeline_call(self, mock_pipe):
        """
        Verify handle_finished_pipeline is called with the correct parameters
        and skipped when it should be.
        """
        params = {'object_kind': 'pipeline',
                  'object_attributes': {'status': 'failed', 'id': 15},
                  'project': {'path_with_namespace': 'thisone',
                              'web_url': 'https://instance/project'}}
        main.process_message(mock.Mock(), params)
        mock_pipe.assert_called_once_with(15, 'thisone', mock.ANY, mock.ANY)

        mock_pipe.reset_mock()
        params['object_attributes']['status'] = 'running'
        main.process_message(mock.Mock(), params)
        mock_pipe.assert_not_called()
