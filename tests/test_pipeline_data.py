"""
Tests for umb_messenger.pipeline_data. Mostly sanity checks to verify content
is set in the correct function.
"""
import copy
import os
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import pipeline_data


class TestFillCommonData(unittest.TestCase):
    """Tests for pipeline_data.fill_common_data()."""
    @mock.patch('umb_messenger.helpers.get_variables')
    def test_sanity(self, mock_variables):
        """Sanity check to verify expected fields are set."""
        message = {'system': [{'os': None}],
                   'run': {'url': None},
                   'generated_at': None}
        mock_variables.return_value = {'kpet_tree_family': 'fedora'}
        pipe = fakes.FakePipeline('success', 1)
        pipe.attributes['web_url'] = 'pipe_url'

        test_message = pipeline_data.fill_common_data(message, pipe)

        self.assertIsNot(test_message['generated_at'], None)
        self.assertIsNot(test_message['system'][0]['os'], None)
        self.assertIsNot(test_message['run']['url'], None)

    def _test_system_os(self, variables, expected):
        message = {'system': [{'os': None}], 'run': {'url': None}, 'generated_at': None}
        pipe = fakes.FakePipeline('success', 1)
        pipe.attributes['web_url'] = 'pipe_url'

        with mock.patch('umb_messenger.helpers.get_variables',
                        mock.Mock(return_value=variables)):
            if expected:
                test_message = pipeline_data.fill_common_data(message, pipe)
                self.assertEqual(test_message['system'][0]['os'], expected)
            else:
                self.assertRaises(Exception, pipeline_data.fill_common_data, message, pipe)

    def test_system_os(self):
        """Check that system.os is correctly filled in."""
        self._test_system_os({'kpet_tree_family': 'fedora'}, 'fedora')
        self._test_system_os({}, None)


class TestFillBaseOSCIData(unittest.TestCase):
    """Tests for pipeline_data.fill_base_osci_data()."""
    @mock.patch('umb_messenger.helpers.get_variables')
    def test_sanity(self, mock_variables):
        """Sanity check to verify expected fields are set."""
        message = {'artifact': {'issuer': None,
                                'id': None,
                                'nvr': None,
                                'scratch': None}}
        mock_variables.return_value = {'submitter': 'me@email.org',
                                       'brew_task_id': 123,
                                       'nvr': 'kernel-123.src.rpm',
                                       'is_scratch': 'False'}
        pipe = fakes.FakePipeline('success', 1)

        test_message = pipeline_data.fill_base_osci_data(message, pipe)

        self.assertEqual(test_message['artifact']['issuer'], 'me@email.org')
        self.assertEqual(test_message['artifact']['id'], 123)
        self.assertFalse(test_message['artifact']['scratch'])
        # Besides checking the value is set, also check the suffix is removed
        # from NVR
        self.assertEqual(test_message['artifact']['nvr'], 'kernel-123')

        # Test also the 'else' branch
        mock_variables.return_value['is_scratch'] = 'True'
        test_message = pipeline_data.fill_base_osci_data(message, pipe)
        self.assertTrue(test_message['artifact']['scratch'])


class TestGatingData(unittest.TestCase):
    """Tests for pipeline_data.get_gating_data()."""
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_missing_architecture(self, mock_data, mock_jobs):
        """Verify error message is set if architecture is missing."""
        message = {'system': [{'architecture': None}]}
        job = fakes.FakeProjectJob(1, 1, {'web_url': 'url'})

        mock_jobs.return_value = [job]
        mock_data.return_value = {}

        messages = pipeline_data.get_gating_data(message, None, None)

        self.assertEqual(len(messages), 1)
        self.assertIn('architecture for url', messages[0]['reason'])

    @mock.patch.dict(os.environ, {'BEAKER_URL': 'beaker.url'})
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_fields(self, mock_data, mock_jobs):
        """
        Verify debug kernel info is added to the message type and that all
        expected fields are set.
        """
        message = {'system': [{'architecture': None, 'os': 'fedora'}],
                   'test': {'type': None, 'status': None},
                   'artifact': {'nvr': 'kernel-123'},
                   'pipeline': {'id': None},
                   'run': {'log': None, 'debug': None, 'rebuild': None}}

        pipe = fakes.FakePipeline('success', 1, {})
        job = fakes.FakeProjectJob(2, 2, {})

        mock_jobs.return_value = [job]
        mock_data.return_value = {
            'state':
                {'retcode': 0,
                 'debug_kernel': 'true',
                 'kernel_arch': 'x86_64',
                 'package_name': 'kernel',
                 'kernel_version': '123'},
            'build': {},
        }

        messages = pipeline_data.get_gating_data(message, pipe, None)

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['test']['type'], 'tier1-x86_64-debug')
        self.assertEqual(messages[0]['test']['result'], 'passed')
        self.assertIn('123%40fedora', messages[0]['run']['log'])
        self.assertEqual(messages[0]['pipeline']['id'], "1-tier1-x86_64-debug")

        # Verify non-debug kernel values
        mock_data.return_value['state']['debug_kernel'] = 'false'
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['test']['type'], 'tier1-x86_64')

        # Verify field values with a failure retcode
        mock_data.return_value['state']['retcode'] = 1
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['test']['result'], 'failed')

        # Verify Beaker errors
        mock_data.return_value['state']['retcode'] = 2
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertIn('Missing test results', messages[0]['error']['reason'])

        # Verify missing data messages
        del mock_data.return_value['state']['retcode']
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertIn('Missing test results', messages[0]['error']['reason'])


class TestReadyForTestData(unittest.TestCase):
    """Tests for pipeline_data.fill_ready_for_test_data()."""
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_pre_test(self, mock_data, mock_jobs):
        """Verify expected values for pre-test notifications are set."""
        template = {'system': [{'os': 'rhel7',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('running', 1, {'patch_urls': 'url1 url2',
                                                 'submitter': 'someone',
                                                 'mr_url': 'mr-url',
                                                 'branch': 'target'})

        mock_jobs.return_value = ['job1', 'job2']
        rc_data = [
            {'state':
                {'debug_kernel': 'true',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel1_url',
                 'modified_files': 'file1',
                 'kpet_tree_name': 'rhel78-z'},
             'build':
                {'build_id': 'redhat:1410268839'},
             },
            {'state':
                {'debug_kernel': 'false',
                 'job_id': '1410268840',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel2_url',
                 'modified_files': 'file1',
                 'kpet_tree_name': 'rhel78-z'},
             'build':
                {'build_id': 'redhat:1410268840'},
             },
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )

        # Check basic data
        self.assertEqual(message['pipelineid'], 1)
        self.assertEqual(message['artifact']['issuer'], 'someone')
        self.assertFalse(message['cki_finished'])
        self.assertNotIn('status', message)
        self.assertEqual(message['patch_urls'], ['url1', 'url2'])
        self.assertEqual(message['modified_files'], ['file1'])
        self.assertEqual(message['system'][0]['stream'], 'rhel78-z')
        self.assertEqual(message['branch'], 'target')

        # Check empty MR URL
        self.assertEqual(message['merge_request']['merge_request_url'], '')

        # Check build info
        self.assertEqual(len(message['build_info']), 2)
        self.assertEqual(
            message['build_info'][0],
            {'architecture': 'x86_64',
             'build_id': 'redhat:1410268839',
             'debug_kernel': True,
             'kernel_package_url': 'kernel1_url'}
        )
        self.assertEqual(
            message['build_info'][1],
            {'architecture': 'x86_64',
             'build_id': 'redhat:1410268840',
             'debug_kernel': False,
             'kernel_package_url': 'kernel2_url'}
        )

        # Check git builds instead of patches
        pipe.variables._vars = [fakes.FakeVariable('branch', 'baseline')]
        del rc_data[0]['state']['modified_files']
        del rc_data[1]['state']['modified_files']
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )

        self.assertEqual(message['artifact']['issuer'], 'CKI')
        self.assertEqual(message['patch_urls'], [])
        self.assertEqual(message['modified_files'], [])
        self.assertEqual(message['branch'], 'baseline')

        # Check MR pipelines
        rc_data[0]['state']['mr_diff_url'] = 'link-to-diff'
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )
        self.assertEqual(message['patch_urls'], ['link-to-diff'])

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_post_test(self, mock_data, mock_jobs):
        """
        Verify expected values for post-test notifications are set. We don't
        need to test both patch and git builds as that's already done for
        pre-test messages and the code is same for both cases.
        """
        template = {'system': [{'os': 'mainline.kernel.org',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('finished', 1, {'skip_results': 'true',
                                                  'branch': 'baseline'})

        mock_jobs.return_value = [fakes.FakePipelineJob(1, 0, {}),
                                  fakes.FakePipelineJob(2, 1, {})]
        rc_data = [
            {'state':
                {'debug_kernel': 'true',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel1_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 0},
             'build':
                {'build_id': 'redhat:1410268839'},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel2_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 0},
             'build':
                {'build_id': 'redhat:1410268840'},
             },
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        # Check basic data
        self.assertTrue(message['cki_finished'])
        self.assertEqual(message['pipelineid'], 1)
        self.assertEqual(message['artifact']['issuer'], 'CKI')
        self.assertEqual(message['status'], 'success')
        self.assertEqual(message['patch_urls'], [])
        self.assertEqual(message['modified_files'], [])
        self.assertEqual(message['system'][0]['stream'], 'upstream')
        self.assertEqual(message['branch'], 'baseline')

        # Check build info
        self.assertEqual(len(message['build_info']), 2)
        self.assertEqual(
            message['build_info'][0],
            {'architecture': 'x86_64',
             'build_id': 'redhat:1410268839',
             'debug_kernel': True,
             'kernel_package_url': 'kernel1_url'}
        )
        self.assertEqual(
            message['build_info'][1],
            {'architecture': 'x86_64',
             'build_id': 'redhat:1410268840',
             'debug_kernel': False,
             'kernel_package_url': 'kernel2_url'}
        )

        # Verify status if any tests failed
        rc_data[1]['state']['retcode'] = 1
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )
        self.assertEqual(message['status'], 'fail')

        # Verify status if some testing was skipped
        del rc_data[1]['state']['retcode']
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )
        self.assertEqual(message['status'], 'success')
        self.assertEqual(len(message['build_info']), 1)

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_post_test_result_success(self, mock_data, mock_jobs):
        """
        Verify expected values for post-test notifications are set if result
        checking is enabled.
        """
        template = {'system': [{'os': 'mainline.kernel.org',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('finished', 1, {'branch': 'branchname'})

        mock_jobs.side_effect = [
            [fakes.FakePipelineJob(1, 0, {}), fakes.FakePipelineJob(2, 1, {})],
            [fakes.FakePipelineJob(3, 2, {'status': 'success'})]
        ]
        # Set retcodes to failures to detect the passing result checking
        # correctly.
        rc_data = [
            {'state':
                {'debug_kernel': 'true',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel1_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 1},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel2_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 1},
             'build': {},
             }
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        self.assertEqual(message['status'], 'success')

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_post_test_result_fail(self, mock_data, mock_jobs):
        """
        Verify expected values for post-test notifications are set if result
        checking is enabled.
        """
        template = {'system': [{'os': 'mainline.kernel.org',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('finished', 1, {'branch': 'testbranch'})

        mock_jobs.side_effect = [
            [fakes.FakePipelineJob(1, 0, {}), fakes.FakePipelineJob(2, 1, {})],
            [fakes.FakePipelineJob(3, 2, {'status': 'this went wrong'})]
        ]
        rc_data = [
            {'state':
                {'debug_kernel': 'true',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel1_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 1},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel2_url',
                 'kpet_tree_name': 'upstream',
                 'retcode': 1},
             'build': {},
             },
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        self.assertEqual(message['status'], 'fail')

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_missing_data(self, mock_data, mock_jobs):
        """
        Verify we don't send messages for builds we can't get info for.
        The cases are:
            - missing architecture info
            - missing kernel URL
            - no test data, if we're sending post-test message
        """
        template = {'system': [{'os': 'fedora',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('finished', 1, {'skip_results': 'true',
                                                  'branch': 'baseline'})

        mock_jobs.return_value = [fakes.FakeProjectJob(i, i, {}) for i in range(2, 6)]
        rc_data = [
            {'state':
                {'debug_kernel': 'true',
                 # kernel_package_url missing
                 'kernel_arch': 'x86_64',
                 'retcode': 1},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_package_url': 'kernel1_url',
                 # kernel_arch missing
                 'retcode': 0},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_package_url': 'kernel2_url',
                 'kernel_arch': 'x86_64',
                 'retcode': 2},  # wrong retcode
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel3_url',
                 'reason': 'unsupported',  # no testing to be done according to kpet
                 'retcode': 0},
             'build': {},
             },
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        self.assertIn('No test data found', message['reason'])

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_broken_first_job(self, mock_data, mock_jobs):
        """Verify we skip jobs with empty rc data."""
        template = {'system': [{'os': 'fedora',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('finished', 1, {'skip_results': 'true',
                                                  'branch': 'baseline'})

        mock_jobs.return_value = [fakes.FakeProjectJob(i, i, {}) for i in range(2, 6)]
        rc_data = [
            {'state': {},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_package_url': 'kernel1_url',
                 'modified_files': 'file list',
                 'kpet_tree_name': 'tree',
                 # kernel_arch missing
                 'retcode': 0},
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_package_url': 'kernel2_url',
                 'kernel_arch': 'x86_64',
                 'modified_files': 'file list',
                 'kpet_tree_name': 'tree',
                 'retcode': 2},  # wrong retcode
             'build': {},
             },
            {'state':
                {'debug_kernel': 'false',
                 'kernel_arch': 'x86_64',
                 'kernel_package_url': 'kernel3_url',
                 'modified_files': 'file list',
                 'kpet_tree_name': 'tree',
                 'retcode': 0},
             'build': {},
             },
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        # Verify the common information is correctly grabbed and the first
        # empty job data is skipped correctly.
        self.assertEqual(message['modified_files'], ['file', 'list'])
        self.assertEqual(message['system'][0]['stream'], 'tree')

    @mock.patch('umb_messenger.pipeline_data.parse_gitlab_url')
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_mr_data(self, mock_data, mock_jobs, mock_parse):
        """Verify MR data for pre-test notifications are correctly set."""
        template = {'system': [{'os': 'rhel7',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None,
                    'branch': None,
                    'merge_request': {
                        'merge_request_url': None
                    }}
        pipe = fakes.FakePipeline('running', 1, {'patch_urls': 'url1 url2',
                                                 'submitter': 'someone',
                                                 'mr_url': 'mr-url',
                                                 'mr_id': 123,
                                                 'branch': 'target'})
        fake_mr = fakes.FakeMergeRequest({
            'iid': 123,
            'description': ('Draft: my mr\nBugzilla: https://bz\nrandom line'
                            'with a Bugzilla: do-not-extract\nBugzilla:'
                            'http://extract-this  \nSigned-off-by: myself'),
            'labels': ['randomlabel', 'Bugzilla::NeedsReview',
                       'Acks::NeedsReview', 'Subsystem:scsi',
                       'Subsystem:qla2xxx'],
            'work_in_progress': True
        })
        fake_project = fakes.FakeGitLabProject()
        fake_project.pipelines._pipelines = [pipe]
        fake_project.mergerequests._mrs = [fake_mr]

        mock_jobs.return_value = []
        mock_data.side_effect = []
        mock_parse.return_value = (None, fake_mr)

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, fake_project
        )

        # Check MR data
        self.assertEqual(message['merge_request']['merge_request_url'], 'mr-url')
        self.assertEqual(message['merge_request']['is_draft'], True)
        self.assertEqual(message['merge_request']['bugzilla'],
                         ['https://bz', 'http://extract-this'])
        self.assertEqual(message['merge_request']['subsystems'],
                         ['scsi', 'qla2xxx'])
        self.assertEqual(message['branch'], 'target')
