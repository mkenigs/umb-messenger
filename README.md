# umb-messenger

Webhook responsible for UMB message sending. Messages can be sent upon
completion of the specified stage or finished pipelines.

## Currently supported messages

* **OSCI:** For details about schema and use case, check out [Fedora CI messages].
  Messages are sent upon pipeline completion, if the pipeline was configured to
  support the messages.

* **Ready for test:** Messages used to trigger testing in specialized labs.
  Meant to be used for testing that is not feasible to onboard into CKI (e.g.
  requires controlled environment or special hardware). A message is sent after
  a kernel is built *and* after CKI functional testing completes.

### Ready for test message schema

```
{
    "ci": {
      "name": "CKI (Continuous Kernel Integration)",
      "team": "CKI",
      "docs": "https://cki-project.org/",
      "url": "https://xci32.lab.eng.rdu2.redhat.com/",
      "email": "cki-project@redhat.com",
      "irc": "#kernelci"
    },
    "run": {
      "url": "<PIPELINE_URL>"
    },
    "artifact": {
      "type": "cki-build",
      "issuer": "<PATCH_AUTHOR_EMAIL or CKI>",  # Always CKI for merge request testing
      "component": "kernel"
    },
    "system": [
      {
        "os": "<kpet_tree_family>",
        "stream": "<kpet_tree_name>"
      }
    ],
    "build_info": [
      {
        "architecture": "<ARCH>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      {
        "architecture": "<ARCH>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      ...
    ],
    "patch_urls": ["list", "of", "strings", "or", "empty", "list"],
    "merge_request": {
      "merge_request_url": "link-or-empty-string",
      "is_draft": bool,
      "subsystems": ["list", "of", "strings"],
      "bugzilla": ["list", "of", "bz", "links"]
    },
    "branch": "name-of-branch-or-empty-string",
    "modified_files": ["list", "of", "strings", "or", "empty", "list"],
    "pipelineid": <ID>,
    "cki_finished": bool,
    "status": <"success" or "fail">,  # attribute NOT present if cki_finished is false
    "category": "kernel-build",
    "namespace": "cki",
    "type": "build",
    "generated_at": "<DATETIME_STRING>",
    "version": "0.1.0"
}
```

For documentation of `kpet_tree_family` and `kpet_tree_name`, see the [CKI
pipeline configuration documentation].

[CKI pipeline configuration documentation]: https://cki-project.org/docs/user_docs/configuration/

### Ready for test details

Messages are being sent to `/topic/VirtualTopic.eng.cki.ready_for_test`. All
labs involved in extra testing will be notified if the topic is changed. The
results should arrive back to `/topic/VirtualTopic.eng.cki.results`, description
of the messages can be found in [receiver's documentation].

Onboarded labs can filter specific messages they are interested in. Some
examples include:

* Filtering on modified files to only test changes to their subsystem
* Filtering on `system/os` to only test changes to specific release
* Filtering on `cki_finished == false` to run testing in parallel
* Filtering on `cki_finished == true && status == 'success'` to e.g. only run
  performance tests if CKI functional testing passed
* Filtering on nonempty `patch_urls` to only test proposed changes instead of
  already merged ones
* Combination of above

## How to run this project

The messenger can be deployed locally by running

```bash
python -m umb_messenger --queue
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

- `SSL_PEM_PATH`: UMB certificate, defaults to `/etc/cki/umb/cki-bot-prod.pem`
- `UMB_CONFIG_PATH`: UMB configuration, defaults to `/etc/cki/umb-config/umb.yml`
- `GITLAB_TOKENS`: JSON dictionary mapping GitLab host names to names of
  environment variables with the corresponding private tokens
- `COM_GITLAB_TOKEN` etc: private GitLab tokens
- `CKI_LOGGING_LEVEL`: logging level for CKI modules, defaults to WARN; to get
  meaningful output on the command line, set to INFO

To receive messages via `--queue` from RabbitMQ, the following environment
variables need to be set as well:

- `RABBITMQ_HOST`: space-separated list of AMQP servers
- `RABBITMQ_PORT`: AMQP port
- `RABBITMQ_USER`: user name of the AMQP account
- `RABBITMQ_PASSWORD`: password of the AMQP account
- `WEBHOOK_RECEIVER_EXCHANGE`: name of the webhook exchange, defaults to
  `cki.exchange.webhooks`
- `UMB_MESSENGER_ROUTING_KEYS`: routing keys corresponding to the subscribed
  webhook events

### Manual triggering

Messaging can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```
CKI_LOGGING_LEVEL=INFO \
  IS_PRODUCTION=true \
  python3 -m umb_messenger \
  --gitlab-url GITLAB_URL \
  --hook-type HOOK_TYPE \
  --id PIPELINE_ID \
  --project PROJECT_NAME
```

* `HOOK_TYPE` can be `job` or `pipeline`. The naming corresponds to mocked
  GitLab webhook. For parallel testing, use `job`. For post-testing messages
  (e.g. OSCI), use `pipeline`.
* `PROJECT_NAME` is a full project name (including the namespace) where the
  pipeline was executed.
* `PIPELINE_ID` is the pipeline ID.

### Production setup

If `IS_PRODUCTION` is set to `true`, [sentry] support is enabled via
`SENTRY_DSN`. You can the value in your sentry project's settings under
`SDK Setup / Client Keys`.


[Fedora CI messages]: https://pagure.io/fedora-ci/messages/
[receiver's documentation]: https://gitlab.com/cki-project/pipeline-trigger/#umb-result-message-format
[sentry]: https://sentry.io/welcome/
