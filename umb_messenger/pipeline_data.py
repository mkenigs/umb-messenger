"""Functionality for retrieving pipeline data for messages."""
from copy import deepcopy
import re

from cki_lib import misc
from cki_lib.gitlab import parse_gitlab_url
from cki_lib.logger import get_logger

from . import helpers

LOGGER = get_logger(__name__)

# Beaker job search parameters
SEARCH_TABLE = 'jobsearch-0.table=Whiteboard'
SEARCH_OPERATION = 'jobsearch-0.operation=contains'
SEARCH_VALUE = 'jobsearch-0.value=cki%40gitlab%3A'


def fill_common_data(message, pipeline):
    """Add pipeline info to the message."""
    variables = helpers.get_variables(pipeline)
    message['system'][0]['os'] = variables['kpet_tree_family']
    message['run']['url'] = pipeline.attributes['web_url']
    message['generated_at'] = misc.utc_now_iso()

    return message


def fill_base_osci_data(message, pipeline):
    """Add OSCI info to the message."""
    variables = helpers.get_variables(pipeline)

    message['artifact']['issuer'] = variables['submitter']
    message['artifact']['id'] = int(variables['brew_task_id'])
    message['artifact']['scratch'] = misc.strtobool(variables['is_scratch'])

    # Remove the .src.rpm suffix from build NVR if it's present
    message['artifact']['nvr'] = variables['nvr'].replace('.src.rpm', '')

    return message


def get_gating_data(template, pipeline, project):
    """Get messages for gating."""
    messages = []
    jobs = helpers.get_newest_jobs(project, pipeline, 'test')

    for job in jobs:
        rc_data = helpers.get_rc_data(job)
        message = deepcopy(template)

        try:
            message['system'][0]['architecture'] = rc_data['state']['kernel_arch']
        except KeyError:
            message['reason'] = 'Unable to find architecture for {}'.format(
                job.attributes['web_url']
            )
            messages.append(message)
            continue

        message['test']['type'] = f"tier1-{message['system'][0]['architecture']}"
        if misc.strtobool(rc_data['state'].get('debug_kernel', 'false')):
            message['test']['type'] += '-debug'

        # We require ALL gating tests to pass in order to mark the testing as
        # successful. This means we need to throw an error if some tests
        # couldn't be executed as that could mean that
        # (1) they crashed because of a kernel bug
        # (2) infra issue prevented them from running but if they ran, they'd
        #     fail because of a bug
        # Of course the tests could pass on rerun but the possiblility of
        # missing a kernel bug requires us to notify the maintainers.
        retcode = rc_data['state'].get('retcode', 2)
        if retcode == 0:
            message['test']['result'] = 'passed'
        elif retcode in [1, 3, 5]:
            message['test']['result'] = 'failed'
        else:
            # Also set the result for completion
            message['test']['result'] = 'unknown'
            message['error'] = {'reason': 'Missing test results for a required test!'}

        # Add links to *all* jobs as some may have aborted.
        beaker_url = misc.get_env_var_or_raise('BEAKER_URL')
        pipeline_id = str(pipeline.attributes['id'])
        full_url = '{}/jobs/?{}&{}&{}{}+{}%40{}+{}'.format(
            beaker_url,
            SEARCH_TABLE,
            SEARCH_OPERATION,
            SEARCH_VALUE,
            pipeline_id,
            rc_data['state']['kernel_version'],
            message['system'][0]['os'],
            message['system'][0]['architecture']
        )
        message['run']['log'] = full_url

        # needs to be unique (e.g. pipeline + job ID), but the same for job retries
        message['pipeline']['id'] = f"{pipeline_id}-{message['test']['type']}"
        message['artifact']['component'] = rc_data['state']['package_name']

        messages.append(message)

    return messages


def fill_ready_for_test_data(message, message_sub_type, pipeline, project):
    # pylint: disable=too-many-branches,too-many-statements,too-many-locals
    """Add test results to the message."""
    variables = helpers.get_variables(pipeline)

    message['pipelineid'] = pipeline.attributes['id']
    # Default to CKI if there's no submitter info (e.g. MRs, baselines)
    message['artifact']['issuer'] = variables.get('submitter', 'CKI')

    if message_sub_type == 'pre':
        message['cki_finished'] = False
        stage = 'setup'
        del message['status']  # Testing didn't finish yet
    else:
        message['cki_finished'] = True
        stage = 'test'

    # Did we run result evaluation?
    skip_results = misc.strtobool(variables.get('skip_results', 'false'))

    jobs = helpers.get_newest_jobs(project, pipeline, stage)
    rc_data_list = [data for data in (helpers.get_rc_data(job) for job in jobs) if data['state']]

    message['patch_urls'] = variables.get('patch_urls', '').split()
    message['branch'] = variables.get('branch', '')

    # Data specific to merge requests. Set to reasonable values and overwrite
    # as needed.
    message['merge_request']['merge_request_url'] = ''
    message['merge_request']['is_draft'] = False
    message['merge_request']['subsystems'] = []
    message['merge_request']['bugzilla'] = []
    if 'mr_url' in variables and 'mr_id' in variables:
        message['merge_request']['merge_request_url'] = variables['mr_url']
        # The merge request may not be on the same project as the pipeline!
        _, mr_object = parse_gitlab_url(variables['mr_url'])
        message['merge_request']['is_draft'] = mr_object.work_in_progress

        # Get BZ info, if provided
        message['merge_request']['bugzilla'] = re.findall(
            r'^Bugzilla:\s*(https?://[^\s]*)[\s]*$',
            mr_object.description,
            re.MULTILINE
        )

        # Get subsystem labels, if any
        message['merge_request']['subsystems'] = [
            label.split(':')[1] for label in mr_object.labels
            if label.startswith('Subsystem:')
        ]

    message['build_info'] = []
    for index, rc_data in enumerate(rc_data_list):
        # skip if kpet decided that an arch should not be tested
        if rc_data['state'].get('reason') == 'unsupported':
            continue

        # If we found no patches then this could be a pipeline for MR or a base
        # kernel. Check if we have the MR diff available to pass along.
        if not message['patch_urls']:
            message['patch_urls'] = rc_data['state'].get('mr_diff_url', '').split()

        debug_kernel = misc.strtobool(rc_data['state'].get('debug_kernel', 'false'))

        partial_build_info = {
            'architecture': rc_data['state'].get('kernel_arch'),
            'build_id': rc_data['build'].get('build_id'),
            'debug_kernel': debug_kernel,
            'kernel_package_url': rc_data['state'].get('kernel_package_url')
        }

        # Sanity check -- do not send info about kernels that can't be
        # identified. The QE would need to drop them anyways. In theory this
        # case should never happen, in practice people sometime retry jobs with
        # expired artifacts and things explode.
        if partial_build_info['architecture'] is None or \
                partial_build_info['kernel_package_url'] is None:
            LOGGER.error('Cannot determine kernel info from job %s',
                         jobs[index].attributes['web_url'])
            continue

        # If we're sending post-test messages and couldn't do any testing for
        # given kernel, skip it. If no testing ran, assume error.
        if skip_results:
            if message['cki_finished'] and rc_data['state'].get('retcode', 2) == 2:
                LOGGER.warning('No test results for %s', jobs[index].attributes['web_url'])
                continue

        message['build_info'].append(partial_build_info)

    # Give up here if there's no kernel data when it should
    if not message['build_info']:
        LOGGER.warning('No data available for pipeline %s!', pipeline.attributes['web_url'])
        message['reason'] = 'No test data found!'
        return message

    modified_files = rc_data_list[0]['state'].get('modified_files', '')
    message['modified_files'] = modified_files.split()

    message['system'][0]['stream'] = rc_data_list[0]['state'].get('kpet_tree_name', '')

    if message['cki_finished']:
        if skip_results:
            # We need to recreate the results based on the retcodes:
            # We dropped error results from build info, we should drop them here
            # too and only summarize the status of testing that actually ran.
            all_retcodes = [rc_data['state'].get('retcode', 2) for rc_data in rc_data_list
                            if rc_data['state'].get('retcode', 2) != 2]
            if all(retcode == 0 for retcode in all_retcodes):
                message['status'] = 'success'
            elif any(retcode in [1, 3, 5] for retcode in all_retcodes):
                message['status'] = 'fail'
            else:
                LOGGER.error('Unexpected retcodes found: %s', all_retcodes)
        else:
            # Only one job runs in this stage.
            result_job = helpers.get_newest_jobs(project, pipeline, 'kernel-results')[0]
            if result_job.attributes['status'] == 'success':
                message['status'] = 'success'
            else:
                message['status'] = 'fail'

    return message
