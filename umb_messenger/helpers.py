"""Helper functions needed by the webhook."""
import itertools

from cki_lib import misc
from cki_lib.logger import get_logger
import gitlab

LOGGER = get_logger(__name__)


def all_jobs_succeeded(project, pipeline, stage, job_name):
    """
    Check if all jobs for given stage succeeded.

    Get the list of newest unique jobs to filter out retried jobs. If job_name
    is not None, also verify that the job with given name is the last one that
    finished.

    Args:
        project:  Project object used by gitlab module.
        pipeline: Pipeline object used by gitlab module.
        stage:    Name of the stage which is being checked.
        job_name: Name of the job that triggered the message, None if the
                  webhook was triggered manually.

    Returns:
        List of unique job objects if the checks are successful,
        empty list otherwise.
    """
    jobs = get_newest_jobs(project, pipeline, stage)
    if not jobs:
        # There are still some running jobs in this stage. No need for extra
        # logging, this is already logged in the get_newest_jobs function.
        return []

    if not all(job.attributes['status'] == 'success' for job in jobs):
        LOGGER.debug('Job with status != success found.')
        return []

    if job_name:
        last_finished = max(jobs, key=lambda j: j.attributes['finished_at'])
        if last_finished.attributes['name'] != job_name:
            LOGGER.debug('Job %s did not finish last!', job_name)
            return []

    return jobs


def get_variables(pipeline):
    """
    Retrieve trigger variables in a reasonable dictionary format.

    This is easier to handle than the list of objects that gitlab module gives
    us.
    """
    variables = {}
    objs = pipeline.variables.list(as_list=False)

    for var in objs:
        variables[var.key] = var.value

    return variables


def get_rc_data(job):
    """
    Retrieve rc data from given job.

    Args:
        job: Project job object for gitlab module. Do not pass a pipeline job
             object as those don't provide access to the artifacts!

    Returns:
        Dictionary of dictionaries with keys and values from rc file.
    """
    try:
        raw_data = job.artifact('rc')
    except gitlab.GitlabGetError:  # The job is broken and doesn't have an rc file
        return {'state': {}, 'build': {}}

    all_data = misc.parse_config_data(raw_data)
    all_data.setdefault('state', {})
    all_data.setdefault('build', {})
    return all_data


def get_newest_jobs(project, pipeline, stage):
    """
    Filter out unique jobs for given stage and pipeline.

    If we retry a job, there will be multiple jobs with the same name in the
    list, but we are only interested in the newest one. This prevents us from
    reporting failures when we shouldn't.

    If there are any unfinished jobs for the stage, empty list is returned.
    This again prevents us from sending out inaccurate messages in case of
    quick retries.

    Querying jobs via pipeline API provides less information about the jobs.
    For consistency and to get all data (e.g. artifacts), the jobs are queried
    via the project interface.

    Args:
        project:  Project object for gitlab module.
        pipeline: Pipeline object for gitlab module.
        stage:    String, name of the stage to retrieve job list for.

    Returns:
        A list of unique finished jobs.
    """
    all_jobs = [job for job in pipeline.jobs.list(as_list=False)
                if job.attributes['stage'] == stage]

    if any(job.attributes['finished_at'] is None for job in all_jobs):
        LOGGER.debug('Job still running, not sending any messages.')
        return []

    # Find jobs we're interested in. Turns out itertools are dumb and we have
    # to first sort the list :facepalm:
    all_jobs.sort(key=lambda j: j.attributes['name'])

    job_ids = []
    for _, group in itertools.groupby(all_jobs,
                                      key=lambda j: j.attributes['name']):
        job_ids.append(max(list(group), key=lambda j: j.attributes['id']).attributes['id'])

    return [project.jobs.get(job_id) for job_id in job_ids]


def should_skip(variables):
    """
    Check pipeline variables for signs of pipelines that should be skipped.

    Args:
        variables: Dictionary of pipeline's trigger variables.

    Returns:
        a string detailing the reason why the pipeline should not be handled,
        None otherwise
    """
    # Retrigger variable is only defined for retriggers, production pipelines
    # don't have it.
    if misc.strtobool(variables.get('retrigger', 'False')):
        return 'retriggered pipeline'

    if 'Patch kickstart' in variables.get('title', ''):
        return 'kickstart pipeline'

    return None
